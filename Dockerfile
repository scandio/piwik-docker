FROM dell/piwik:1.2
MAINTAINER Scandio GmbH

# Environment variables for container initialization
ENV INIT_DIRECTORY			/docker/artifactory/init
ENV TEMP_DIRECTORY			/docker/artifactory/temp

# Create init and temp directory
RUN mkdir -p ${INIT_DIRECTORY} && mkdir -p ${TEMP_DIRECTORY}

# Copy start script and set correct permissions
COPY init/* ${INIT_DIRECTORY}/
RUN chmod 740 ${INIT_DIRECTORY}/*.sh

# Install necessary packages
RUN apt-get update && apt-get install -y \
		openssh-client \
		rsync \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

# Execute start script
CMD ${INIT_DIRECTORY}/configure-and-start.sh