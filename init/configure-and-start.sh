#!/bin/bash

########################################################################
# Variables for Piwik
########################################################################

piwik_start_script=/run.sh

########################################################################
# Variables for backup restore
########################################################################

${RESTORE_BACKUP:=false}
backup_identity_file="${TEMP_DIRECTORY}/id_backup"

########################################################################
# Main script
########################################################################

# Restore backup from remote host if requested
if [ "${RESTORE_BACKUP}" != "false" ]; then
	touch ${backup_identity_file} && chmod 600 ${backup_identity_file} && printf -- "${BACKUP_SSH_KEY}" > ${backup_identity_file}
	
	${BASH_SOURCE%/*}/restore-backup.sh -s ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/mysql/* -t /var/lib/mysql/ -i ${backup_identity_file}
	chown -R mysql:mysql /var/lib/mysql

	${BASH_SOURCE%/*}/restore-backup.sh -s ${BACKUP_USER}@${BACKUP_HOST}:${BACKUP_PATH}/html/* -t /var/www/html/ -i ${backup_identity_file}
	chown -R www-data:www-data /var/www/html/config
	chown -R www-data:www-data /var/www/html/tmp

	rm ${backup_identity_file}
	unset BACKUP_SSH_KEY
fi

# Run Piwik
exec ${piwik_start_script}